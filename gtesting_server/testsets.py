import json
from typing import Union

from gtesting_server_shared import TestSet

from gtesting_server.modules import redis_client


class exceptions:

    class TestSetExists(Exception):

        pass


def load(_id: str) -> Union[TestSet, None]:
    r = redis_client.get_redis()
    if ts_bytes := r.get(_id):
        ts_obj = json.loads(ts_bytes)
        return TestSet.Schema().load(ts_obj)
    return None


def save(ts: TestSet):
    r = redis_client.get_redis()
    if load(ts._id):
        raise exceptions.TestSetExists()
    ts_obj = TestSet.Schema().dump(ts)
    r.set(ts._id, json.dumps(ts_obj))
