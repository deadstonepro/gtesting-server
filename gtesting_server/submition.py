import uuid
import asyncio
from typing import Iterable

from marshmallow_dataclass import class_schema

from gtesting_server_shared import Test, TestSet, TestReport, SubmitionReport

from .publisher import Publisher
from .testing_strategy import TestingStrategy


class Submition:

    _id: str
    _strategy: TestingStrategy
    _publisher: Publisher
    _report: SubmitionReport
    _source: str
    _tests: Iterable[Test]
    _time_limit: float

    @property
    def id(self):
        return self._id

    def __init__(self, strategy: object, source: str, testset: TestSet):
        assert isinstance(strategy, (TestingStrategy,))
        self._id = str(uuid.uuid1())
        self._strategy = strategy
        self._source = source
        self._tests = testset.tests
        self._time_limit = testset.time_limit
        self._report = SubmitionReport(self._id)
        report_view = SubmitionReport.Schema().dump
        self._publisher = Publisher(report_view, self._report)

    async def subscribe(self, *argv, **kwargs):
        await self._publisher.subscribe(*argv, **kwargs)

    async def __call__(self) -> SubmitionReport:
        if errs := (await asyncio.shield(self._strategy.prepare(self._source))):
            self._report.status = SubmitionReport.Status.FAILED
            self._report.messages = errs
            await self._publisher.update(self._report)
            return self._report

        self._report.status = SubmitionReport.Status.COMPILATION
        await self._publisher.update(self._report)

        if errs := (await asyncio.shield(self._strategy.compile())):
            self._report.status = SubmitionReport.Status.COMPILATION_FAILED
            self._report.messages = errs
            await self._publisher.update(self._report)
            return self._report

        self._report.status = SubmitionReport.Status.RUNNING
        await self._publisher.update(self._report)

        for test in self._tests:
            try:
                task = self._strategy.run(test)
                test_report = await asyncio.wait_for(task, timeout=self._time_limit)
            except asyncio.TimeoutError:
                test_report = TestReport(verdict=TestReport.Verdict.TL)

            self._report.reports.append(test_report)
            await self._publisher.update(self._report)

        self._report.status = SubmitionReport.Status.FINISHED
        await self._publisher.update(self._report)
        return self._report
