from pathlib import Path

from .generic_fs_strategy import GenericFSTestingStrategy


class Python3FsTestingStrategy(GenericFSTestingStrategy):

    def __init__(self, testing_dir: Path):

        def compile_command_factory(input_path: Path, exec_path: Path):
            return " ".join(("python3", "-m", "py_compile", str(input_path.resolve()),
                "&&", "cp", str(input_path.resolve()), str(exec_path.resolve())))

        def execute_command_factory(exec_path: Path):
            return " ".join(("python3", str(exec_path.resolve())))

        super().__init__(testing_dir, compile_command_factory, execute_command_factory)
