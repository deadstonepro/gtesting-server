from pathlib import Path

from .generic_fs_strategy import GenericFSTestingStrategy


class CppFsTestingStrategy(GenericFSTestingStrategy):

    def __init__(self, testing_dir: Path):

        def compile_command_factory(input_path: Path, exec_path: Path):
            cxx_options = ("-O2", "-std=c++17",)
            return " ".join(("g++", "-x", "c++", *cxx_options, str(input_path.resolve()),
                "-o", str(exec_path.resolve())))

        def execute_command_factory(exec_path: Path):
            return str(exec_path.resolve())

        super().__init__(testing_dir, compile_command_factory, execute_command_factory)
