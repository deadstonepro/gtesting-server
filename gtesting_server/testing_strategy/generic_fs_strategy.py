import uuid
import asyncio
from enum import Enum
from pathlib import Path
from typing import List, Callable

from gtesting_server_shared import Test, TestReport

from .testing_strategy import TestingStrategy


class State(Enum):
    INIT = 0
    PREPARED = 1
    COMPILED = 2


class GenericFSTestingStrategy(TestingStrategy):

    compile_command_factory: Callable[[str, str], str]
    execute_command_factory: Callable[[str], str]

    testing_dir    : Path
    submition_dir  : Path
    source_path    : Path
    exec_path      : Path
    state: State = State.INIT

    def __init__(self, testing_dir: Path,
        compile_command_factory: Callable[[str, str], str],
        execute_command_factory: Callable[[str], str]):
        self.compile_command_factory = compile_command_factory
        self.execute_command_factory = execute_command_factory
        self.testing_dir = testing_dir

    async def prepare(self, source: str) -> List[str]:
        assert self.state == State.INIT
        self.submition_dir = self.testing_dir / str(uuid.uuid1())
        self.submition_dir.mkdir(parents=True)
        self.source_path = self.submition_dir / "source"
        with self.source_path.open("w") as source_file:
            source_file.write(source)
        self.state = State.PREPARED
        return list()

    async def compile(self) -> List[str]:
        assert self.state == State.PREPARED
        self.exec_path = self.submition_dir / "exec"
        proc = await asyncio.create_subprocess_shell(
            self.compile_command_factory(self.source_path, self.exec_path),
            stdout=asyncio.subprocess.PIPE,
            stderr=asyncio.subprocess.PIPE)
        _, stderr = await proc.communicate()
        if proc.returncode != 0:
            return [f"rc: {proc.returncode}", stderr.decode()]
        self.state = State.COMPILED
        return list()

    async def run(self, test: Test) -> TestReport:
        assert self.state == State.COMPILED
        proc = await asyncio.create_subprocess_shell(
            self.execute_command_factory(self.exec_path),
            stdin=asyncio.subprocess.PIPE,
            stdout=asyncio.subprocess.PIPE,
            stderr=asyncio.subprocess.PIPE)
        stdout, stderr = await proc.communicate(" ".join(test.input).encode())
        if proc.returncode != 0:
            return TestReport(TestReport.Verdict.RE, [stderr.decode()])
        if stdout.decode().split() != test.output:
            return TestReport(TestReport.Verdict.WA)
        return TestReport(TestReport.Verdict.OK)
