from typing import Union
from json.decoder import JSONDecodeError
import functools

from marshmallow import Schema, fields
from aiohttp import web

from gtesting_server_shared import TestSet

from .modules import tasks_pool
from .application import config_var
from .submition import Submition
from . import testing_strategy as strategy
from . import testsets


routes = web.RouteTableDef()


def json_api(req_schema: Union[Schema, None], resp_schema: Union[Schema, None]):
    def wrapper(method):
        @functools.wraps(method)
        async def decorated(request, *args, **kwargs):
            try:
                request_obj = await request.json()
            except (web.HTTPBadRequest, JSONDecodeError) as exc:
                return web.Response(
                    status=500,
                    text=f"Couldn't parse the request: {exc}")

            if validation_errs := req_schema.validate(request_obj):
                return web.Response(
                    status=500,
                    text=f"Bad request: {validation_errs}")

            result = await method(request_obj, *args, **kwargs)
            if isinstance(result, (web.Response,)):
                return result
            return web.json_response(resp_schema.dump(result))

        return decorated

    return wrapper


def get_strategy(language: str, _: TestSet) -> Union[strategy.TestingStrategy, None]:
    execution_dir = config_var.get().RUNNERS_DIR
    if language == "python3":
        return strategy.Python3FsTestingStrategy(execution_dir)
    if language in ["cpp", "cxx"]:
        return strategy.CppFsTestingStrategy(execution_dir)
    return None


@routes.post("/testset")
@json_api(TestSet.Schema(exclude=("_id",)), TestSet.Schema())
async def testset_handler(request):
    testset = TestSet(**request)
    testsets.save(testset)
    return testset


def format_url_template(template: str, **kwargs) -> str:
    for (key, value) in kwargs.items():
        template = template.replace(f"${key}", value)
    return template


class SubmitReqSchema(Schema):

    testset_id = fields.Str()
    language = fields.Str()
    source = fields.Str()
    callback_url = fields.Url(required=False)


class SubmitRespSchema(Schema):

    submition_id = fields.Str()


@routes.post("/submit")
@json_api(SubmitReqSchema(), SubmitRespSchema())
async def submit(request):
    if not (testset := testsets.load(request["testset_id"])):
        return web.Response(status=404, text="Test set not found.")
    if not (strategy := get_strategy(request["language"], testset)):
        return web.Response(status=500, text="Couldn't find a testing strategy.")
    submition = Submition(strategy, request["source"], testset)
    if callback_url := request.get("callback_url"):
        await submition.subscribe(callback_url)
    tasks_pool.schedult(submition)
    return { "submition_id": submition.id }


class SubcribeReqSchema(Schema):

    submition_id = fields.Str()
    callback_url = fields.Url()


@routes.post("/subscribe")
@json_api(SubcribeReqSchema(), None)
async def subscribe(request):
    try:
        submition = tasks_pool.get(lambda t: t.id == request["submition_id"])
    except LookupError:
        return web.Response(status=404, text="Submition not found.")
    await submition.subscribe(request["callback_url"])
    return web.Response(status=200)
