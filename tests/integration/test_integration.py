import uuid
import json
import unittest
import asyncio
from pathlib import Path
from typing import Dict, Tuple

from aiohttp import web, ClientSession

from gtesting_server_shared import SubmitionReport, TestReport, TestSet

from gtesting_server.routes import routes, SubmitRespSchema
from gtesting_server.application import create_app, app_context
from gtesting_server.modules import tasks_pool, redis_client


class Config:

    class Server:
        PROT = "http"
        HOST = "localhost"
        PORT = 8080
        URL = f"{PROT}://{HOST}:{PORT}"

    class Client:
        PROT = "http"
        HOST = "localhost"
        PORT = 8081
        URL = f"{PROT}://{HOST}:{PORT}"


class ClientServerFixture(unittest.IsolatedAsyncioTestCase):

    server: web.Application
    server_runner: web.AppRunner
    client_runner: web.AppRunner

    submition_states: Dict[str, SubmitionReport]

    async def __run_server(self):
        self.server = create_app(["--config", "config:TestingConfig"], routes)
        tasks_pool.init_app(self.server)
        redis_client.init_app(self.server)
        self.server_runner = web.AppRunner(self.server)
        await self.server_runner.setup()
        site = web.TCPSite(self.server_runner, Config.Server.HOST, Config.Server.PORT)
        await site.start()

    async def __run_client(self):
        app = web.Application()
        app.add_routes([web.post("/", self.__submition_updated)])
        self.client_runner = web.AppRunner(app)
        await self.client_runner.setup()
        site = web.TCPSite(self.client_runner, Config.Client.HOST, Config.Client.PORT)
        await site.start()

    async def __submition_updated(self, request):
        req_obj = await request.json()
        report = SubmitionReport.Schema().load(req_obj)
        submition_id = report.submition_id
        self.submition_states[submition_id] = report
        return web.Response(status=200)

    async def asyncSetUp(self):
        self.submition_states = dict()
        self.subscription_to_submition = dict()
        await self.__run_server()
        await self.__run_client()

    async def asyncTearDown(self):
        with app_context(self.server):
            r = redis_client.get_redis()
            for key in r.keys("*"):
                r.delete(key)
        await self.server_runner.cleanup()
        await self.client_runner.cleanup()

    async def _wait_until_report(self, sub_id: str):
        while sub_id not in self.submition_states.keys():
            await asyncio.sleep(0.2)

    async def _wait_unitl_tested(self, sub_id: str):
        terminal_states = [
            SubmitionReport.Status.FINISHED,
            SubmitionReport.Status.COMPILATION_FAILED,
            SubmitionReport.Status.FAILED
        ]
        await self._wait_until_report(sub_id)
        while self.submition_states[sub_id].status not in terminal_states:
            await asyncio.sleep(0.2)


def load_test_source(data_dir: Path) -> Tuple[str, Dict]:
    with (data_dir / "source").open("r") as f:
        source = f.read()
    with (data_dir / "testset.json").open("r") as f:
        ts = json.load(f)
    return (source, ts)


async def upload_testset(ts: Dict) -> TestSet:
    async with ClientSession() as s:
        async with s.post(f"{Config.Server.URL}/testset", json=ts) as resp:
            resp_obj = await resp.json()
            return TestSet.Schema().load(resp_obj)


async def submit(submition: Dict) -> str:
    async with ClientSession() as s:
        async with s.post(f"{Config.Server.URL}/submit", json=submition) as resp:
            resp_obj = await resp.json()
            return SubmitRespSchema().load(resp_obj)["submition_id"]


async def subscribe(subscription: Dict) -> str:
    async with ClientSession() as s:
        async with s.post(f"{Config.Server.URL}/subscribe", json=subscription) as resp:
            pass


class IntegrationTest(ClientServerFixture):

    async def _execute(self, data_dir: Path, lang: str) -> Tuple[str, TestSet]:
        src, testset_assets = load_test_source(data_dir)
        testset = await upload_testset(testset_assets)
        submition = {
            "source": src,
            "language": lang,
            "testset_id": testset.id,
            "callback_url": f"{Config.Client.URL}"
        }
        sub_id = await submit(submition)
        return (sub_id, ts)

    async def test_basic(self):
        data_dir = Path(__file__).resolve().parent / "data" / "matrix_multiplication"
        sub_id, ts = await self._execute(data_dir, lang="python3")
        await asyncio.wait_for(self._wait_unitl_tested(sub_id), timeout=10)
        report = self.submition_states[sub_id]
        self.assertEqual(report.status, SubmitionReport.Status.FINISHED)
        self.assertEqual(len(report.reports), len(ts.tests))
        self.assertTrue(all(test.verdict == TestReport.Verdict.OK 
                            for test in report.reports))

    async def test_wno_autosubscription(self):
        data_dir = Path(__file__).resolve().parent / "data" / "matrix_multiplication"
        src, ts = load_test_source(data_dir)
        ts = await upload_testset(ts)
        submition = {
            "source": src,
            "language": "python3",
            "testset_id": ts._id,
        }
        sub_id = await submit(submition)
        subscription = {
            "submition_id": sub_id,
            "callback_url": f"{Config.Client.URL}"
        }
        await subscribe(subscription)
        await asyncio.wait_for(self._wait_unitl_tested(sub_id), timeout=10)
        report = self.submition_states[sub_id]
        self.assertEqual(report.status, SubmitionReport.Status.FINISHED)
        self.assertEqual(len(report.reports), len(ts.tests))
        self.assertTrue(all(test.verdict == TestReport.Verdict.OK 
            for test in report.reports))

    async def test_compile_error(self):
        data_dir = Path(__file__).resolve().parent / "data" / "matrix_multiplication_ce"
        sub_id, ts = await self._execute(data_dir, lang="python3")
        await asyncio.wait_for(self._wait_unitl_tested(sub_id), timeout=10)
        report = self.submition_states[sub_id]
        self.assertEqual(report.status, SubmitionReport.Status.COMPILATION_FAILED)

    async def test_time_limit(self):
        data_dir = Path(__file__).resolve().parent / "data" / "matrix_multiplication_tl"
        sub_id, ts = await self._execute(data_dir, lang="python3")
        await asyncio.wait_for(self._wait_unitl_tested(sub_id), timeout=10)
        report = self.submition_states[sub_id]
        self.assertEqual(report.status, SubmitionReport.Status.FINISHED)
        self.assertTrue(any(test_report.verdict == TestReport.Verdict.TL for test_report in report.reports))

    async def test_basic_cpp(self):
        sub_id, _ = await self._execute(
            Path(__file__).resolve().parent / "data" / "matrix_multiplication_cpp",
            lang="cpp")
        await asyncio.wait_for(self._wait_unitl_tested(sub_id), timeout=10)
        report = self.submition_states[sub_id]
        self.assertEqual(report.status, SubmitionReport.Status.FINISHED)
