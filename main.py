from gtesting_server.routes import routes
from gtesting_server.application import create_app
from gtesting_server.modules import redis_client, tasks_pool

def main(argv):
    app = create_app(argv, routes)
    modules = (
        redis_client,
        tasks_pool
    )
    for module in modules:
        module.init_app(app)
    return app
